const express = require('express');
const bodyParser = require('body-parser');
const restful = require('node-restful');
const errorHandler = require('./src/errorHandler')

var dotenv = require('dotenv');
if( dotenv ) dotenv.load();

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

const mongoose = require('mongoose');
mongoose.Promise = global.Promise
mongoose.connect(process.env.MONGODB_URL, {useMongoClient: true});

mongoose.Error.messages = {
	general: {
		required: "The attibute '{PATH}' is mandatory."
	},
	Number: {
		min: "'{VALUE}' is lower then '{MIN}'.",
		max: "'{VALUE}' is greather than '{MAX}'."
	},
	String: {
		enum: "'{VALUE}' is not valid for attribute '{PATH}'."
	}
}


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', function() {
    console.log("Connected to Database");
});

const docs = require("express-mongoose-docs");
docs(app, mongoose);

var QuoteTemplate = {
    name: { type: String, required: true, index: true },
    quote: String,
    number: { type: Number, min: 18, max: 65, required: true },
};
var QuoteSchema = mongoose.Schema(QuoteTemplate);
app.quote = restful.model('quote', QuoteSchema).methods(['get', 'post', 'put', 'delete']);
app.quote.after('post', errorHandler).after('put', errorHandler)
app.quote.register(app, '/quote');



app.listen(3000);
