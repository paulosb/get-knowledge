"use strict";
const gulp = require('gulp');
const jasmine = require('gulp-jasmine-node');
const jshint = require("gulp-jshint");

const srcFiles = [
	'server.js'
];
const testFiles = [
	//'spec/**/*.js'
	'spec/sample_spec.js'
];
const srcAndTestFiles = srcFiles.concat(testFiles)

gulp.task('test', ['jasmine']);

gulp.task('jsLint', function () {
	gulp.src(srcAndTestFiles) // path to your files
		.pipe(jshint())
		.pipe(jshint.reporter()); // Dump results
});

gulp.task('jasmine', () => {
	gulp.src(testFiles)
		.pipe(jasmine({
			timeout: 1000,
			includeStackTrace: false,
			showColors: true,
			verbose: true
		}))
});
gulp.task('watch', () => {
	gulp.watch(srcAndTestFiles, ['jsLint', 'jasmine'])
	
});
